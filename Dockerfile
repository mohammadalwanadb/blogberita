FROM php:7.4-apache
WORKDIR /var/www/html
COPY . /var/www/html
RUN apt-get update && apt-get install -y \
    libpng-dev \
    zlib1g-dev \
    libxml2-dev \
    libzip-dev \
    libonig-dev \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install zip \
    && docker-php-source delete

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN chown -R www-data:www-data /var/www/html \
    && a2enmod rewrite

RUN composer update
COPY .env.example .env
RUN sed -i 's/DB_HOST=.*/DB_HOST=192.168.0.13/' /var/www/html/.env
RUN sed -i 's/DB_DATABASE.*/DB_DATABASE=blog/' /var/www/html/.env
RUN sed -i 's/DB_USERNAME.*/DB_USERNAME=alwan/' /var/www/html/.env
RUN sed -i 's/DB_PASSWORD.*/DB_PASSWORD=alwan/' /var/www/html/.env
RUN php artisan key:generate
CMD php artisan key:generate && php artisan migrate && php artisan db:seed && php artisan serve --host=0.0.0.0 --port=80
